import bpy, bmesh, mathutils, sys
from . import utils
from . import utils_gl

DEBUG_ = 0

class CreateDataException(Exception):
    pass

class WPLocalWeightsTransfer(bpy.types.Operator):
    """Transfer vertex weights based on selection"""
    bl_idname = "local_weights_trasnfer.transfer"
    bl_label = "Local Weights Transfer"
    bl_options = {'REGISTER', 'UNDO'}

    mode: bpy.props.EnumProperty(
        name = "Mode",
        items = ( ('s2a', "From selected to active", "Transfer weights from selected to active island"),
                  ('u2s', "From unselected to selected", "Transfer weights from unselected to selected"),
                  ('ts2a', "From transformable selected to active", "Transfer weights from transformed selected to active island"),
                ),
        default = 'u2s',
        options = {'HIDDEN'},
    )

    source_subset: bpy.props.EnumProperty(
        name = "Source Subset",
        items = ( ('ACTIVE', "Active Group", "Transfers weights only from the active vertex group"),
                  ('ALL', "All Groups", "Transfers weights from all vertex groups"),
                  ('DEFORM_GROUPS', "Deform Groups", "Transfers weights from all vertex groups associated with deforming bones"),
                ),
        default = 'ACTIVE',
    )

    target_subset: bpy.props.EnumProperty(
        name = "Target Subset",
        items = ( ('SAME', "Same Groups", "Transfers weights within the source groups"),
                  ('OTHER', "Other Groups", "Transfers weights from source groups to other groups"),
                ),
        default = 'SAME',
    )

    text_source: bpy.props.StringProperty(default="")
    text_target: bpy.props.StringProperty(default="")


    matching: bpy.props.EnumProperty(
        name = "Vertex matching",
        items = ( ('NEAREST_FACE', "Nearest Face Interpolated", "Matches vertices with Nearest Face Interpolated method"),
                  ('PROJECTED_FACE', "Projected Face Interpolated", "Matches vertices with Projected Face Interpolated method"),
                ),
        default = 'NEAREST_FACE',
    )

    with_inverted_normals: bpy.props.BoolProperty(
         name = "Inverted normals",
         default = False,
         description = "Use inverted vertex normals for Projected Face Interpolated matching",
    )

    factor: bpy.props.FloatProperty(
        name = "Factor",
        description = "A factor of 1.0 fully replaces old weights; 0.0 keeps old weights",
        default = 1.0,
        min = 0.0,
        max = 1.0,
    )

    grow_factor: bpy.props.FloatProperty(
        name = "Expand/Contract",
        description= "Expand/Contract weights",
        default = 0.0,
        min = -1.0,
        max = 1.0,
    )

    radius: bpy.props.FloatProperty(
        name = "Radius",
        description = "Maximum distance to search for faces to transfer weights from. Set to 0 for no limit",
        default = 0.0,
        min = 0.0,
        soft_max = 8.0,
    )

    ob = None
    initial_vg_active_ind = None
    bm = None
    free_bm = True
    g_target = None
    g_source = None
    bvh = None
    bvh_original_face_idx = None

    draw_state = None
    mat = None

    @classmethod
    def poll(cls, context):
        ob = context.active_object
        return  ob and ob.type=='MESH' and \
                ( (ob.mode == 'WEIGHT_PAINT' and (ob.data.use_paint_mask_vertex or ob.data.use_paint_mask) ) or \
                  (ob.mode == 'EDIT' and (context.scene.tool_settings.mesh_select_mode[0])) )

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False

        box = layout.box()
        row = box.row()
        row.prop(self, "source_subset")
        row = box.row()
        row.prop(self, "target_subset")

        if self.target_subset == 'OTHER':
            if self.source_subset == 'ACTIVE':
                row = box.row()
                row.prop_search(self, "text_target", context.active_object, "vertex_groups", text="Target vertex group")
            else:
                row = box.row()
                row.prop(self, "text_source", text="Find")
                row = box.row()
                row.prop(self, "text_target", text="Replace")

        row = layout.row()
        row.prop(self, "matching")
        if self.matching == 'PROJECTED_FACE':
            row = layout.row()
            row.prop(self, "with_inverted_normals")
        row = layout.row()
        row.prop(self, "factor")
        row = layout.row()
        row.prop(self, "grow_factor")
        row = layout.row()
        row.prop(self, "radius")

    def transfer_weights(self, vg_source, vg_target):
        ob_data = self.ob.data
        if (self.radius < 1e-6):
            max_dist = sys.float_info.max
        else:
            max_dist = self.radius
        with_nearest_face = (self.matching == 'NEAREST_FACE')
        current_weights = {}

        def get_curr_weight(v_idx):
            w = current_weights.get(v_idx, None)
            if w is None:
                w = utils.get_weight(ob_data.vertices[v_idx], vg_source.index)[1]
                current_weights[v_idx] = w
            return w

        idxs_to_add = []
        targets_vg_idx = []
        for v_idx in self.g_target:
            vg_idx, w = utils.get_weight(ob_data.vertices[v_idx], vg_target.index)
            if vg_idx is None:
                idxs_to_add.append(v_idx)
                vg_idx = -1
            current_weights[v_idx] = w
            targets_vg_idx.append(vg_idx)

        if len(idxs_to_add) > 0:
            vg_target.add(idxs_to_add, 0.0, 'REPLACE')
        del idxs_to_add

        for i, v_idx in enumerate(self.g_target):
            if with_nearest_face:
                loc, normal, bvh_f_idx, dist = self.bvh.find_nearest( ob_data.vertices[v_idx].co, max_dist )
            else:
                v = ob_data.vertices[v_idx]
                if not self.with_inverted_normals:
                    direction = v.normal
                else:
                    direction = -v.normal
                loc, normal, bvh_f_idx, dist = self.bvh.ray_cast( v.co, direction, max_dist )

            if loc is None:
                # ~ new_w = 0
                continue
            else:
                f_idx = self.bvh_original_face_idx[bvh_f_idx]
                poly_co = []
                poly_w = []
                for v_idx2 in ob_data.polygons[f_idx].vertices:
                    co = ob_data.vertices[v_idx2].co
                    if self.mat is not None:
                        co = self.mat @ co
                    poly_co.append( co )
                    poly_w.append( get_curr_weight(v_idx2) )
                poly_p = mathutils.interpolate.poly_3d_calc( poly_co, loc )
                new_w = sum( p * w for p,w in zip(poly_p, poly_w) )

            curr_w = current_weights[v_idx]
            if self.grow_factor > 0.0:
                new_w = self.grow_factor * max(new_w, curr_w) + (1-self.grow_factor) * new_w
            else:
                new_w = -self.grow_factor * min(new_w, curr_w) + (1+self.grow_factor) * new_w
            new_w = self.factor * new_w + (1-self.factor) * curr_w

            ob_data.vertices[v_idx].groups[ targets_vg_idx[i] ].weight = new_w

    def execute(self, context):
        if DEBUG_ > 1:
            print("  execute()")
        r = self.do_transfer(context)
        self.report( {'INFO'}, "Modified {} groups.".format(r) )
        return {'FINISHED'}

    def do_transfer(self, context):
        mode0 = context.active_object.mode
        if mode0 != 'WEIGHT_PAINT':
            bpy.ops.object.mode_set(mode='WEIGHT_PAINT')

        self.ob = context.active_object
        ob_vgs = self.ob.vertex_groups
        paired_vgs_names = set()

        def get_vgs_pair(source_vg):
            if self.target_subset == 'SAME':
                return (source_vg, source_vg)
            if self.source_subset == 'ACTIVE':
                vg = ob_vgs.get(self.text_target, None)
                if vg is None:
                    return None
                return (source_vg, vg)

            target_vg_name = source_vg.name.replace(self.text_source, self.text_target, 1)
            if target_vg_name == source_vg.name:
                return None
            if target_vg_name in paired_vgs_names:
                return None

            vg = ob_vgs.get(target_vg_name, None)
            if vg is None:
                return None
            return (source_vg, vg)

            # ~ new_vg = ob_vgs.new(name=target_vg_name)
            # ~ return (source_vg, new_vg)

        if (self.target_subset == 'OTHER'):
            if (self.source_subset == 'ACTIVE'):
                if len(self.text_target.strip()) == 0:
                    if mode0 != 'WEIGHT_PAINT':
                        bpy.ops.object.mode_set(mode=mode0)
                    return 0
            elif len(self.text_source.strip()) == 0:
                if mode0 != 'WEIGHT_PAINT':
                    bpy.ops.object.mode_set(mode=mode0)
                return 0

        source_vgs = None
        if self.source_subset == 'ACTIVE':
            source_vgs = [ ob_vgs[self.initial_vg_active_ind] ]
        elif self.source_subset == 'ALL':
            source_vgs = [ vg for vg in ob_vgs ]
        elif self.source_subset == 'DEFORM_GROUPS':
            source_vgs = utils.deforming_vgs(self.ob)

        vgs_pairs = []
        if DEBUG_ > 0:
            print("\nVgs pairs:")
            for vg in source_vgs:
                print("  \"{}\" (index: {}) ".format(vg.name, vg.index), end="")
                pair = get_vgs_pair(vg)
                if pair is not None:
                    vgs_pairs.append(pair)
                    paired_vgs_names.add(pair[0].name)
                    paired_vgs_names.add(pair[1].name)
                    print("---> \"{}\" (index: {}).".format(pair[1].name, pair[1].index))
                else:
                    print("---> no pair found (skipped).")
            print()
        else:
            for vg in source_vgs:
                pair = get_vgs_pair(vg)
                if pair is not None:
                    vgs_pairs.append(pair)
                    paired_vgs_names.add(pair[0].name)
                    paired_vgs_names.add(pair[1].name)

        for vg_source, vg_target in vgs_pairs:
            self.transfer_weights(vg_source, vg_target)

        r = len(vgs_pairs)
        if r > 0:
            ob_vgs.active = vgs_pairs[0][1]
        else:
            ob_vgs.active_index = self.initial_vg_active_ind

        if mode0 != 'WEIGHT_PAINT':
            bpy.ops.object.mode_set(mode=mode0)
        return r

    def create_bvh(self):
        bvh_vertices_index = {}
        bvh_vertices = []
        bvh_polygons = []
        bvh_original_face_idx = {}

        is_s2a = (self.mode in ('s2a', 'ts2a'))

        for f in self.bm.faces:
            if f.hide:
                continue
            if is_s2a:
                if (not f.select) or (f.verts[0].index in self.g_target):
                    continue
            else:
                if f.select:
                    continue

            polygon_indices = []
            for v in f.verts:
                if v.index not in bvh_vertices_index:
                    idx = len(bvh_vertices)
                    if self.mat is not None:
                        v_co = self.mat @ v.co
                    else:
                        v_co = v.co
                    bvh_vertices.append(v_co)
                    bvh_vertices_index[v.index] = idx
                else:
                    idx = bvh_vertices_index[v.index]
                polygon_indices.append(idx)

            bvh_original_face_idx[ len(bvh_polygons) ] = f.index
            bvh_polygons.append(polygon_indices)

        if len(bvh_polygons) == 0:
            self.report({'ERROR'}, "Invalid selection: no source faces found.")
            raise CreateDataException

        # ~ print("bvh vertices: {} \n\n".format(str(bvh_vertices)))
        self.bvh = mathutils.bvhtree.BVHTree.FromPolygons(bvh_vertices, bvh_polygons)
        self.bvh_original_face_idx = bvh_original_face_idx

    def find_source_target_s2a(self, is_face_mask):
        vi_active = None
        if not is_face_mask:
            vi_active = utils.get_active_vert(self.bm)
        else:
            vi_active = utils.get_active_face_vert(self.ob)

        if vi_active is None:
            e = None
            if is_face_mask:
                e = "face"
            else:
                e = "vertex"
            self.report({'ERROR'}, "Invalid selection: no active {} selected.".format(e))
            raise CreateDataException

        g_active = set()
        g_active.update( utils.visit_island_selected(self.bm, vi_active) )
        if len(g_active) == 0:
            self.report({'ERROR'}, "Invalid selection: no active island.")
            raise CreateDataException

        g_source = None
        if self.mode == 'ts2a':
            g_source = set()
            for vi, v in enumerate(self.bm.verts):
                if v.select and (not v.hide) and (vi not in g_active):
                    g_source.add(vi)
            if len(g_source) == 0:
                self.report({'ERROR'}, "Invalid selection: no selected islands.")
                raise CreateDataException

        self.g_source, self.g_target = g_source, g_active

    def find_source_target_u2s(self):
        g_selected = set()
        for vi, v in enumerate(self.bm.verts):
            if (v.select) and (not v.hide):
                g_selected.add(vi)
        if len(g_selected) == 0:
            self.report({'ERROR'}, "Invalid selection: no selected vertices.")
            raise CreateDataException

        self.g_source, self.g_target = None, g_selected

    def create_data(self, context):
        if DEBUG_ > 1:
            print("    create_data()")

        self.mat = None
        self.ob = context.active_object
        if len(self.ob.vertex_groups) == 0:
            self.report({'ERROR'}, "Mesh has no vertex groups.")
            return 0

        self.initial_vg_active_ind = self.ob.vertex_groups.active_index

        if (self.ob.mode != 'EDIT'):    # 'WEIGHT_PAINT'
            self.bm = bmesh.new()       # can be freed
            self.bm.from_mesh(self.ob.data)
            self.bm.verts.ensure_lookup_table()
            self.free_bm = True
        else:
            self.bm = bmesh.from_edit_mesh(self.ob.data) # do not free
            self.free_bm = False

        try:
            if self.mode in ('s2a', 'ts2a'):
                self.find_source_target_s2a(self.ob.data.use_paint_mask)
            else:
                self.find_source_target_u2s()

            if DEBUG_ > 0:
                print()
                print("mode: {}".format(self.mode))
                print("target: " + str(self.g_target))
                if self.g_source is not None:
                    print("source: " + str(self.g_source))

            if self.mode == 'ts2a':
                return 2
            else:
                self.create_bvh()
                self.cleanup_bm()
                return 1

        except CreateDataException:
            self.cleanup_bm()
            return 0

    def cleanup_bm(self):
        if (self.bm is not None) and self.free_bm:
            self.bm.free()
        self.bm = None

    def modal(self, context, event):
        try:
            r = self.draw_state.modal_handler(context, event)
        except Exception as e:
            self.draw_state.end()
            self.cleanup_bm()
            raise e

        if 'RUNNING_MODAL' in r:
            return r
        if 'PASS_THROUGH' in r:
            return r

        if 'CANCELLED' in r:
            self.draw_state.end()
            self.cleanup_bm()
            return r

        if 'FINISHED' in r:
            self.mat = self.draw_state.end()
            self.create_bvh()
            self.cleanup_bm()
            return self.execute(context)

    def invoke(self, context, event):
        if DEBUG_ > 1:
            print("-"*8)
            print("  invoke(), mode={}".format(self.mode))

        r = self.create_data(context)
        if r == 0:
            return {'CANCELLED'}
        elif r == 1:
            return self.execute(context)
        else:
            assert(r==2)

            if context.area.type != 'VIEW_3D':
                self.report({'WARNING'}, "3d view not found.")
                return {'CANCELLED'}

            self.draw_state = utils_gl.DrawState(context, self.g_source)
            self.draw_state.start()

            context.window_manager.modal_handler_add(self)
            return {'RUNNING_MODAL'}
