import bpy, gpu, bmesh
from gpu_extras.batch import batch_for_shader
from bpy_extras.view3d_utils import region_2d_to_location_3d, region_2d_to_vector_3d, location_3d_to_region_2d
from mathutils import Vector, Matrix
import math

pass_keys = {'NUMPAD_0', 'NUMPAD_1', 'NUMPAD_3', 'NUMPAD_4',
                'NUMPAD_5', 'NUMPAD_6', 'NUMPAD_7', 'NUMPAD_8',
                'NUMPAD_9', 'NUMPAD_2',
                'MIDDLEMOUSE', 'WHEELUPMOUSE',
                'WHEELDOWNMOUSE', 'MOUSEMOVE'}

alt_keys = {'MIDDLEMOUSE', 'MOUSEMOVE', 'INBETWEEN_MOUSEMOVE'}

num_keys = {'NUMPAD_0', 'NUMPAD_1', 'NUMPAD_3', 'NUMPAD_4',
            'NUMPAD_5', 'NUMPAD_6', 'NUMPAD_7', 'NUMPAD_8',
            'NUMPAD_9', 'NUMPAD_2', 'ONE', 'TWO', 'ZERO',
            'THREE', 'FOUR', 'FIVE', 'SIX', 'SEVEN', 'EIGHT',
            'NINE', 'TEN',
            'MINUS', 'NUMPAD_MINUS',
            'PLUS', 'NUMPAD_PLUS',
            'PERIOD', 'NUMPAD_PERIOD',
            'BACK_SPACE'}


vertex_shader = '''
    uniform mat4 matrix;

    in vec3 pos;

    void main()
    {
        gl_Position = matrix * vec4(pos, 1.0f);
    }
'''

fragment_shader = '''
    uniform vec4 color;
    out vec4 FragColor;

    void main()
    {
        FragColor = color;
    }
'''


class DrawManager:
    def __init__(self, ob, r3d, g_source):
        self.draw_handler = None
        coords, indices, pos_original = self.get_mesh_data(ob, g_source)

        self.shader = gpu.types.GPUShader(vertex_shader, fragment_shader)
        self.color = (1.0, 0.0, 0.0, 1.0)
        self.batch = batch_for_shader(self.shader, 'LINES', {"pos": coords}, indices=indices)

        self.mat_0 = r3d.perspective_matrix

        self.mat_tras_step = Matrix.Identity(4)
        self.mat_rot_step = Matrix.Identity(4)
        self.mat_sca_step = Matrix.Identity(4)

        self.mat_tras_hist = [ Matrix.Translation(pos_original) ]
        self.mat_rot_hist = [ Matrix.Translation(-pos_original) ]
        self.mat_sca_hist = [ Matrix.Identity(4) ]

        self.hist_max_lvl = 8
        self.hist_index = 0

        self.use_depth_mask = False

    def get_mesh_data(self, ob, g_source):
        curr_mode = ob.mode
        if curr_mode != 'EDIT':
            bpy.ops.object.mode_set(mode='EDIT', toggle=False)
        bm = bmesh.from_edit_mesh(ob.data)

        ob_m_global = ob.matrix_world
        coords = []
        pos_original = Vector((0,0,0))
        inserted_vis = {}

        for v in bm.verts:
            if v.index in g_source:
                inserted_vis[v.index] = len(coords)
                co = tuple(ob_m_global @ v.co)
                coords.append(co)
                pos_original += Vector(co)

        pos_original = pos_original * (1.0 / len(coords))

        indices = []
        for e in bm.edges:
            e_indices = [ inserted_vis[v.index] for v in e.verts if v.index in g_source ]
            if len(e_indices) == 2:
                indices.append( tuple(e_indices) )

        if curr_mode != 'EDIT':
            bpy.ops.object.mode_set(mode = curr_mode, toggle=False)
        return coords, indices, pos_original

    def draw(self):
        self.shader.bind()

        mat = self.mat_0 @ self.get_mat_tras() @ self.get_mat_sca() @ self.get_mat_rot()
        self.shader.uniform_float("matrix", mat)
        self.shader.uniform_float("color", self.color)

        if self.use_depth_mask:
            gpu.state.depth_test_set('LESS_EQUAL')
            gpu.state.depth_mask_set(True)
        else:
            gpu.state.depth_mask_set(False)
        # ~ gpu.state.blend_set('ALPHA')
        self.batch.draw(self.shader)
        # ~ gpu.state.blend_set('NONE')
        gpu.state.depth_mask_set(False)

    def cleanup(self):
        if self.draw_handler is not None:
            bpy.types.SpaceView3D.draw_handler_remove(self.draw_handler, 'WINDOW')
            self.draw_handler = None
            return True
        return False

    def start_draw(self):
        self.cleanup()
        self.draw_handler = bpy.types.SpaceView3D.draw_handler_add(self.draw, (), 'WINDOW', 'POST_VIEW')

    def get_mat_rot(self):
        return self.mat_rot_step @ self.mat_rot_hist[self.hist_index]

    def get_mat_tras(self):
        return self.mat_tras_step @ self.mat_tras_hist[self.hist_index]

    def get_mat_sca(self):
        return self.mat_sca_step @ self.mat_sca_hist[self.hist_index]

    def get_mat(self):
        mat = self.get_mat_tras() @ self.get_mat_sca() @ self.get_mat_rot()
        return mat

    def undo_hist(self):
        if self.hist_index > 0:
            self.hist_index -= 1

    def redo_hist(self):
        if (self.hist_index + 1) < len(self.mat_tras_hist):
            self.hist_index += 1

    def translate(self, pos):
        self.mat_tras_step = Matrix.Translation(pos) @ self.mat_tras_step

    def rotate(self, ang, rot_dir):
        self.mat_rot_step = Matrix.Rotation(ang, 4, rot_dir) @ self.mat_rot_step

    def scale(self, scale, sc_dir):
        if sc_dir is None:
            self.mat_sca_step = Matrix.Scale(scale, 4)
        else:
            self.mat_sca_step = Matrix.Scale(scale, 4, sc_dir)

    def cleanup_tras_step(self):
        self.mat_tras_step = Matrix.Identity(4)

    def cleanup_rot_step(self):
        self.mat_rot_step = Matrix.Identity(4)

    def cleanup_sca_step(self):
        self.mat_sca_step = Matrix.Identity(4)

    def confirm_step(self):
        mat_tras = self.get_mat_tras()
        mat_rot = self.get_mat_rot()
        mat_sca = self.get_mat_sca()

        if self.hist_index == (self.hist_max_lvl-1):
            self.mat_tras_hist = [ self.mat_tras_hist[0] ] + self.mat_tras_hist[2:]
            self.mat_rot_hist = [ self.mat_rot_hist[0] ] + self.mat_rot_hist[2:]
            self.mat_sca_hist = [ self.mat_sca_hist[0] ] + self.mat_sca_hist[2:]
        else:
            self.mat_tras_hist = self.mat_tras_hist[:self.hist_index+1]
            self.mat_rot_hist = self.mat_rot_hist[:self.hist_index+1]
            self.mat_sca_hist = self.mat_sca_hist[:self.hist_index+1]
            self.hist_index += 1

        self.mat_tras_hist.append( mat_tras )
        self.mat_rot_hist.append( mat_rot )
        self.mat_sca_hist.append( mat_sca )

        self.cleanup_tras_step()
        self.cleanup_rot_step()
        self.cleanup_sca_step()

    def get_curr_pos(self):
        return self.get_mat_tras().to_translation()


def mult_components(a, b):
    return Vector([i*j for i,j in zip(a,b)])

def cross_product_z(a, b):
    return a[0]*b[1] - a[1]*b[0]


class DrawState:
    draw_inst = None

    mode = None
    mode_axis = None
    axis_dir = {}
    axis_dir['X'] = Vector((1,0,0))
    axis_dir['Y'] = Vector((0,1,0))
    axis_dir['Z'] = Vector((0,0,1))

    ob = None
    g_source = None
    pos_before_step = None

    pointer_pos_0 = None

    rot_origin = None
    rot_view_dir = None
    rot_dir = None
    pointer_rot_dir_0 = None

    take_num_input = False
    num_sign = '+'
    num_val = ""
    has_num = False
    has_num_prev = False

    pointer_radius_0 = None
    pointer_radius_orthog_0 = None
    pointer_radius_sign_0 = None

    main_header_text = "G: Move; R: Rotate; S: Scale; T: Toggle depth occlusion; Ctrl-Z: Undo; Ctrl-Shift-Z: Redo; Left click: Confirm; ESC: Cancel"
    mode_header_text_prefix = "X,Y,Z: Limit to axis; ESC: Cancel"


    def __init__(self, context, g_source):
        self.ob = context.active_object
        self.draw_inst = DrawManager(self.ob, context.space_data.region_3d, g_source)
        self.pos_before_step = self.draw_inst.get_curr_pos()

    def modal_handler(self, context, event):
        context.area.tag_redraw()

        if self.mode is None:
            context.area.header_text_set(self.main_header_text)

            if (event.type in pass_keys) or event.alt:
                return {'PASS_THROUGH'}

            # ~ if event.type == 'MOUSEMOVE':
            if event.type == 'G':
                self.mode = 'MOVE'
                self.mode_axis = None
                self.clear_num()
                self.pos_before_step = self.draw_inst.get_curr_pos()
                self.pointer_pos_0 = Vector((event.mouse_region_x, event.mouse_region_y))

                # ~ r3d = context.space_data.region_3d
                # ~ self.pointer_pos_0 = region_2d_to_location_3d(context.region, r3d, pos_delta, self.pos_before_step)

            elif event.type == 'R':
                self.pos_before_step = self.draw_inst.get_curr_pos()
                t = (event.mouse_region_x, event.mouse_region_y)
                r3d = context.space_data.region_3d
                self.rot_origin = location_3d_to_region_2d(context.region, r3d, self.pos_before_step)
                if self.rot_origin is None:
                    return {'RUNNING_MODAL'}

                self.mode = 'ROTATE'
                self.mode_axis = None
                self.clear_num()

                # ~ self.rot_view_dir = region_2d_to_vector_3d(context.region, r3d, self.rot_origin)
                self.rot_view_dir = r3d.view_rotation @ Vector((0,0,-1))
                self.pointer_rot_dir_0 = (Vector(t) - self.rot_origin).normalized()

                self.rot_dir = self.rot_view_dir

            elif event.type == 'S':
                self.pos_before_step = self.draw_inst.get_curr_pos()
                r3d = context.space_data.region_3d
                self.rot_origin = location_3d_to_region_2d(context.region, r3d, self.pos_before_step)
                if self.rot_origin is None:
                    return {'RUNNING_MODAL'}

                self.mode = 'SCALE'
                self.mode_axis = None
                self.clear_num()

                t = Vector((event.mouse_region_x, event.mouse_region_y))
                pointer_offset = (t - self.rot_origin)
                self.pointer_radius_orthog_0 = pointer_offset.orthogonal()
                self.pointer_radius_0 = pointer_offset.length
                self.pointer_radius_sign_0 = (cross_product_z(pointer_offset, self.pointer_radius_orthog_0) > 0)

            elif event.type == 'Z' and event.value == 'PRESS' and event.ctrl:
                if event.shift:
                    self.draw_inst.redo_hist()
                else:
                    self.draw_inst.undo_hist()
                return {'RUNNING_MODAL'}

            elif event.type == 'T' and event.value == 'PRESS':
                self.draw_inst.use_depth_mask = not self.draw_inst.use_depth_mask

            elif event.type == 'ESC':
                context.area.header_text_set(None)
                return {'CANCELLED'}

            elif event.value == 'RELEASE' and event.type in {'LEFTMOUSE', 'SELECTMOUSE', 'RET'}:
                context.area.header_text_set(None)
                return {'FINISHED'}

        elif self.take_num_input and event.value == 'PRESS' and event.type in num_keys:
            if event.type in {'MINUS', 'NUMPAD_MINUS', 'PLUS', 'NUMPAD_PLUS'}:
                self.num_sign = event.unicode
            elif event.type == 'BACK_SPACE':
                self.num_val = self.num_val[:-1]
                self.has_num = (len(self.num_val) > 0)
            else:
                self.num_val += event.unicode
                self.has_num = True
            return {'RUNNING_MODAL'}

        elif self.mode == 'ROTATE':
            context.area.header_text_set( self.mode_header_text_prefix + self.txt_num() )

            if event.value == 'RELEASE' and event.type in {'LEFTMOUSE', 'SELECTMOUSE', 'RET'}:
                self.mode = None
                self.mode_axis = None
                self.draw_inst.confirm_step()
                return {'RUNNING_MODAL'}

            elif event.value == 'PRESS' and event.type in self.axis_dir:
                if self.mode_axis is not None and self.mode_axis == event.type:
                    self.mode_axis = None
                    self.rot_dir = self.rot_view_dir
                    self.clear_num()
                else:
                    self.mode_axis = event.type
                    self.rot_dir = self.axis_dir[event.type]
                    self.take_num_input = True
                return {'RUNNING_MODAL'}

            elif event.value == 'RELEASE' and event.type == 'ESC':
                self.mode = None
                self.mode_axis = None
                self.clear_num()
                self.draw_inst.cleanup_rot_step()
                return {'RUNNING_MODAL'}

            else:
                if (self.mode_axis is not None) and (self.has_num):
                    self.draw_inst.cleanup_rot_step()
                    self.has_num_prev = True
                    self.draw_inst.rotate( math.radians(self.val_num()), self.rot_dir )
                else:
                    if self.has_num_prev:
                        self.draw_inst.cleanup_rot_step()
                        self.has_num_prev = False
                    t = Vector( (event.mouse_region_x, event.mouse_region_y) )
                    pointer_rot_dir = (t - self.rot_origin).normalized()
                    pointer_rot_ang = pointer_rot_dir.angle(self.pointer_rot_dir_0) * 0.8
                    if cross_product_z(pointer_rot_dir, self.pointer_rot_dir_0) > 0:
                        pointer_rot_ang *= -1

                    self.pointer_rot_dir_0 = pointer_rot_dir

                    if abs(pointer_rot_ang) > 1e-6:
                        self.draw_inst.rotate(pointer_rot_ang, self.rot_dir)
                return {'RUNNING_MODAL'}

        elif self.mode == 'MOVE':
            context.area.header_text_set( self.mode_header_text_prefix + self.txt_num() )

            if event.value == 'RELEASE' and event.type in {'LEFTMOUSE', 'SELECTMOUSE', 'RET'}:
                self.mode = None
                self.mode_axis = None
                self.draw_inst.confirm_step()
                return {'RUNNING_MODAL'}

            elif event.value == 'PRESS' and event.type in self.axis_dir:
                if self.mode_axis is not None and self.mode_axis == event.type:
                    self.mode_axis = None
                    self.clear_num()
                else:
                    self.mode_axis = event.type
                    self.take_num_input = True
                return {'RUNNING_MODAL'}

            elif event.value == 'RELEASE' and event.type == 'ESC':
                self.mode = None
                self.mode_axis = None
                self.clear_num()
                self.draw_inst.cleanup_tras_step()
                return {'RUNNING_MODAL'}

            else:
                if (self.mode_axis is not None) and (self.has_num):
                    self.draw_inst.cleanup_tras_step()
                    pos_next = self.val_num() * self.axis_dir[self.mode_axis]
                    self.has_num_prev = True
                else:
                    if self.has_num_prev:
                        self.draw_inst.cleanup_tras_step()
                        self.has_num_prev = False
                    pointer_new_pos = Vector( (event.mouse_region_x, event.mouse_region_y) )
                    pointer_offset = pointer_new_pos - self.pointer_pos_0
                    self.pointer_pos_0 = pointer_new_pos

                    r3d = context.space_data.region_3d

                    pos_curr = self.draw_inst.get_curr_pos()
                    pointer_pos_ob = location_3d_to_region_2d(context.region, r3d, pos_curr)
                    pos_next = region_2d_to_location_3d(context.region, r3d, pointer_pos_ob + pointer_offset, pos_curr)

                    if self.mode_axis is not None:
                        pos_next = mult_components(pos_next - pos_curr, self.axis_dir[self.mode_axis])
                    else:
                        pos_next = pos_next - pos_curr

                self.draw_inst.translate(pos_next)
                return {'RUNNING_MODAL'}

        elif self.mode == 'SCALE':
            context.area.header_text_set( self.mode_header_text_prefix + self.txt_num() )
            self.take_num_input = True

            if event.value == 'RELEASE' and event.type in {'LEFTMOUSE', 'SELECTMOUSE', 'RET'}:
                self.mode = None
                self.mode_axis = None
                self.draw_inst.confirm_step()
                return {'RUNNING_MODAL'}

            elif event.value == 'PRESS' and event.type in self.axis_dir:
                if self.mode_axis is not None and self.mode_axis == event.type:
                    self.mode_axis = None
                    self.clear_num()
                    self.take_num_input = True
                else:
                    self.mode_axis = event.type
                return {'RUNNING_MODAL'}

            elif event.value == 'RELEASE' and event.type == 'ESC':
                self.mode = None
                self.mode_axis = None
                self.clear_num()
                self.draw_inst.cleanup_sca_step()
                return {'RUNNING_MODAL'}

            else:
                if self.has_num:
                    self.draw_inst.cleanup_sca_step()
                    scale = self.val_num()
                    self.has_num_prev = True
                else:
                    if self.has_num_prev:
                        self.draw_inst.cleanup_sca_step()
                        self.has_num_prev = False

                    t = Vector((event.mouse_region_x, event.mouse_region_y))
                    pointer_offset = (t - self.rot_origin)
                    pointer_radius = pointer_offset.length
                    # ~ pointer_radius_delta = pointer_radius - self.pointer_radius_0
                    # ~ self.pointer_radius_0 = pointer_radius_delta

                    scale = ( pointer_radius / self.pointer_radius_0 )
                    sign = (cross_product_z(pointer_offset, self.pointer_radius_orthog_0) > 0)
                    if sign != self.pointer_radius_sign_0:
                        scale *= -1

                if self.mode_axis is not None:
                    esc_dir = self.axis_dir[self.mode_axis]
                else:
                    esc_dir = None

                self.draw_inst.scale(scale, esc_dir)

                return {'RUNNING_MODAL'}

        return {'RUNNING_MODAL'}

    def clear_num(self):
        self.num_sign = '+'
        self.num_val = ""
        self.has_num = False
        self.take_num_input = False

    def txt_num(self):
        if self.take_num_input and self.has_num:
            return "; VALUE: {}{}".format(self.num_sign, self.num_val)
        return ""

    def val_num(self):
        txt = self.num_val
        if txt.count('.') < 2:
            return float(self.num_sign + txt)
        i = txt.find('.')
        j = txt.find('.', i+1, len(txt))
        return float(self.num_sign + txt[0:j])

    def start(self):
        assert( self.draw_inst is not None )
        self.draw_inst.start_draw()

    def end(self):
        mat = None
        if self.draw_inst is not None:
            mat = self.draw_inst.get_mat()
            self.draw_inst.cleanup()
            self.draw_inst = None
        return mat
