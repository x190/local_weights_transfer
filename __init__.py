import bpy
from . import ops

bl_info = {
    'name': 'Local Vertex Weights Transfer',
    'author': 'Xin',
    'version': (1, 0, 0),
    'blender': (3, 6, 0),
    'location': 'Weight Paint (vertex/face mode) > Weights > Local Weights Transfer',
    'description': 'Transfers vertex weights in weight paint mode based on selection',
    "doc_url": "https://gitlab.com/x190/local_weights_transfer",
    "tracker_url": "https://gitlab.com/x190/local_weights_transfer",
    'category': 'Paint'
}

class TransferWeightsSubmenuW(bpy.types.Menu):
    bl_label = "Local Weights Transfer"
    bl_idname = "VIEW3D_MT_wp_transfer_menu"

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.operator( ops.WPLocalWeightsTransfer.bl_idname,
                      text="From Unselected To Selected" ).mode='u2s'

        row = layout.row()
        row.operator( ops.WPLocalWeightsTransfer.bl_idname,
                      text="From Selected To Active Island" ).mode='s2a'

        row = layout.row()
        row.operator( ops.WPLocalWeightsTransfer.bl_idname,
                      text="From Transformable Selected To Active Island" ).mode='ts2a'


class TransferWeightsSubmenuE(bpy.types.Menu):
    bl_label = "Local Weights Transfer"
    bl_idname = "VIEW3D_MT_edit_transfer_menu"

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.operator( ops.WPLocalWeightsTransfer.bl_idname,
                      text="From Unselected To Selected" ).mode='u2s'

        row = layout.row()
        row.operator( ops.WPLocalWeightsTransfer.bl_idname,
                      text="From Selected To Active Island" ).mode='s2a'

        row = layout.row()
        row.operator( ops.WPLocalWeightsTransfer.bl_idname,
                      text="From Transformable Selected To Active Island" ).mode='ts2a'



def panel_funcW(self, context):
    layout = self.layout
    layout.separator()
    row = layout.row()
    row.menu(TransferWeightsSubmenuW.bl_idname)


def panel_funcE(self, context):
    layout = self.layout
    layout.separator()
    row = layout.row()
    row.menu(TransferWeightsSubmenuE.bl_idname)


classes = (
    ops.WPLocalWeightsTransfer,
    TransferWeightsSubmenuW,
    TransferWeightsSubmenuE,
)


def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.VIEW3D_MT_paint_weight.append(panel_funcW)
    bpy.types.VIEW3D_MT_edit_mesh.append(panel_funcE)


def unregister():
    bpy.types.VIEW3D_MT_edit_mesh.remove(panel_funcE)
    bpy.types.VIEW3D_MT_paint_weight.remove(panel_funcW)
    for c in reversed(classes):
        bpy.utils.unregister_class(c)


if __name__ == "__main__":
    register()
