import bpy, bmesh

def get_active_vert(bm):
    if bm.select_history:
        e = bm.select_history[-1]
        if isinstance(e, bmesh.types.BMVert) \
           and e.select and (not e.hide):
            return e.index
    return None

def get_active_face_vert(ob):
    fi_active = ob.data.polygons.active
    if fi_active is not None:
        f = ob.data.polygons[fi_active]
        if f.select and (not f.hide):
            return f.vertices[0]
    return None

def get_weight(v, affected_vg_idx):
    for vg_idx, g in enumerate(v.groups):
        if g.group == affected_vg_idx:
            return vg_idx, g.weight
    return None, 0.0

def deforming_vgs(ob):
    bones_names = set()
    for modif in ob.modifiers:
        if modif.type == 'ARMATURE' and modif.object is not None:
            for b in modif.object.data.bones:
                if b.use_deform:
                    bones_names.add(b.name)
    deforming_vgs = []
    if len(bones_names) > 0:
        for vg in ob.vertex_groups:
            if vg.name in bones_names:
                deforming_vgs.append(vg)
    return deforming_vgs

def visit_island_selected(bm, vi):
    visited = set()
    remaining = [vi]
    while len(remaining) > 0:
        vi2 = remaining.pop()
        visited.add(vi2)

        v2 = bm.verts[vi2]
        for e2 in v2.link_edges:
            if e2.select:
                v3 = e2.other_vert(v2)
                if (v3.index not in visited) and v3.select and (not v3.hide):
                    remaining.append(v3.index)
    return visited
